package com.atlassian.marketplace.client.encoding;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Defines the format strings used for date and date/time values in the Marketplace REST API.
 * @since 2.0.0
 */
public abstract class DateFormats
{
    private DateFormats()
    {
    }
    
    /**
     * The format used for all string representations of dates that do not have time values or time zones.
     */
    public static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd");

    /**
     * The format used for all string representations of dates that also have time values.
     */
    public static DateTimeFormatter DATE_TIME_FORMAT = ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC);
}
