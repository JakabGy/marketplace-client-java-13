package com.atlassian.marketplace.client.api;

import io.atlassian.fugue.Either;

import java.util.Optional;

import static com.atlassian.marketplace.client.util.Convert.iterableOf;

/**
 * Encapsulates parameters that can be passed to {@link Addons#safeGetVersion}.
 * @since 2.0.0
 */
public final class AddonVersionSpecifier
{
    private final Optional<Either<String, Long>> nameOrBuild;
    
    private AddonVersionSpecifier(Optional<Either<String, Long>> nameOrBuild)
    {
        this.nameOrBuild = nameOrBuild;
    }
    
    /**
     * Searches for an add-on version by build number.
     */
    public static AddonVersionSpecifier buildNumber(long buildNumber)
    {
        return new AddonVersionSpecifier(Optional.of(Either.right(buildNumber)));
    }
    
    /**
     * Searches for an add-on version by name (version string, e.g. "1.0.0").
     */
    public static AddonVersionSpecifier versionName(String name)
    {
        return new AddonVersionSpecifier(Optional.of(Either.left(name)));
    }
    
    /**
     * Searches for the latest version (which may be constrained by criteria such as compatibility
     * if specified with {@link AddonVersionsQuery}).
     */
    public static AddonVersionSpecifier latest()
    {
        return new AddonVersionSpecifier(Optional.empty());
    }
    
    /**
     * Used internally to access the previously set details of the specifier.  
     */
    public Optional<Either<String, Long>> getSpecifiedVersion()
    {
        return nameOrBuild;
    }
    
    @Override
    public String toString()
    {
        for (Either<String, Long> vob: iterableOf(nameOrBuild))
        {
            for (Long b: vob.right())
            {
                return "buildNumber(" + b + ")";
            }
            for (String n: vob.left())
            {
                return "name(" + n + ")";
            }
        }
        return "latest";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof AddonVersionSpecifier) && ((AddonVersionSpecifier) other).nameOrBuild.equals(this.nameOrBuild);
    }
    
    @Override
    public int hashCode()
    {
        return nameOrBuild.hashCode();
    }
}
