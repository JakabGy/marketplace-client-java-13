/**
 * Entities that can be returned by the Atlassian Marketplace API.
 * <p>
 * Characteristics of these classes:
 * <ul>
 * <li> They are all immutable.
 * <li> No property getter method will ever return <tt>null</tt>.  Optional properties use the
 * {@code Option} type to represent either a value or "none".
 * <li> Any string properties that support HTML markup will use the
 * {@link com.atlassian.marketplace.client.model.HtmlString} class; if you want to display any
 * string property in a browser that does not use {@code HtmlString}, you should HTML-escape it first. 
 * <li> All {@code DateTime} values are UTC. Date properties that are not associated with a time or
 * time zone use {@code LocalDate} instead.
 * <li> Many entities have a <tt>getLinks()</tt> method that returns an object containing REST
 * API links. Links with special meanings can be accessed through convenience methods on the
 * model objects; avoid using the links map directly.
 * <li> Constructors are package-private and are not meant to be used directly.  Use
 * {@link com.atlassian.marketplace.client.model.ModelBuilders} if you need to construct instances of these classes.
 * </ul>
 * <p>
 * Classes that encapsulate parameters to be passed to an API, rather than objects returned
 * from the server, are in the {@link com.atlassian.marketplace.client.api} package.
 */
package com.atlassian.marketplace.client.model;